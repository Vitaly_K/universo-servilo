extends Node


# Порт сервера
const DEFAULT_PORT = 44444

# Максимальное количество клиентов
const MAX_PLAYERS = 10

# Словарь с подключенными клиентами { id: name }
var online_players = {}

# Сигнал посылаемый при регистрации пользователя (см. func register_player)
signal player_registered
# Сигнал посылаемый при отключении пользователя (см. func unregister_player)
signal player_unregistered
# Сигнал посылаемый при получении команды от игрока
signal mining_exec

# Процедура посылает сообщение одному (id != null) пользователю 
# либо всем (player_id == null) пользователям
func send_message(id = null, message = ''):
	if id:
		if id > 0:
			rpc_id(id, 'messaging', message)
	else:
		rpc('messaging', message)


func _ready():
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self,"_player_disconnected")
	
	create_server()


func create_server():
	var host = NetworkedMultiplayerENet.new()
	host.create_server(DEFAULT_PORT, MAX_PLAYERS)
	get_tree().set_network_peer(host)


# Callback from SceneTree, called when client connects
func _player_connected(_id):
	print("Пользователь %s подключился" % _id)


# Callback from SceneTree, called when client disconnects
func _player_disconnected(id):
	rpc("unregister_player", id)
	print("Пользователь %s отключился " % id)


# Регистрация нового игрока
remote func register_player(new_player_name):
	# Получаем ID игрока таким способом вместо того, чтобы передавать через параметры
	# для защиты от подмены игрока
	var caller_id = get_tree().get_rpc_sender_id()

	# Добавляем в список зарегистрированных клиентов
	online_players[caller_id] = new_player_name
	
	# Передаём всех зарегистрированных игроков вновь прибывшему игроку
	for p_id in online_players:
		rpc_id(caller_id, "register_player", p_id, online_players[p_id]) # Send each player to new dude
	
	# Передаём нового игрока всем зарегистрированным
	rpc("register_player", caller_id, online_players[caller_id])
	# NOTE: получается, что инфа о новом игроке прилетит ему самому дважды
	
	print("Пользователь %s зарегистрирован как %s" % [caller_id, new_player_name])
	
	emit_signal("player_registered", new_player_name, caller_id)


# Подчищаем информацию об отлючающемся пользователе
puppetsync func unregister_player(id):
	# Получаем имя пользователя
	var player_name = online_players[id]
	
	print("Пользователь %s (%s) вышел" % [id, online_players[id]])

	# Удаляем пользователя из списка зарегистрированных
	online_players.erase(id)
	
	emit_signal("player_unregistered", player_name)


# Возбуждение сигналов в зависимости от полученной от игрока команды
remote func exec_command(command):
	# Получаем ID игрока, который вызвал эту процедуру
	var id = get_tree().get_rpc_sender_id()
	
	# Комамнда "Запуск добычи руды"
	if command == 'start_mining':
		emit_signal("mining_exec", id)
