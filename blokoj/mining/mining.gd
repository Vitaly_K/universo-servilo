extends Node


# Генератор случайных чисел
var rnd = RandomNumberGenerator.new()
# Игроки, корабли которых выполняют добычу { name: id }
# При выходе игрока до завершения задания id = -1
var mining_players = {}


# Эмуляция добычи руды
# player_name - имя (логин) игрока запустившего задачу
func create_mining_task(player_name):
#	var player_id = mining_players[player_name]
	# Количество собрааной руды, кг
	var m = rnd.randi_range(1005, 1505)
	
	var c = rnd.randi_range(900, 1100)
	# Стоимость собранной руды
	var i = m * c / 1000
	
	# Время затраченное на вылет корабля со станции
	var delay = rnd.randi_range(3, 6)
	yield(get_tree().create_timer(delay), "timeout")
	print("Корабль тов. %s вылетел со станции! (Времязатраты: %s ч.)" % [player_name, delay])
	Net.send_message(mining_players[player_name], 
				 "Корабль вылетел со станции! (Времязатраты: %s ч.)" % delay)

	# Время затраченное на полёт к астероидам
	delay = rnd.randi_range(21, 27)
	yield(get_tree().create_timer(delay), "timeout")
	print("Корабль тов. %s подлетел к астероидам! (Времязатраты: %s ч.)" % [player_name, delay])
	Net.send_message(mining_players[player_name],
				 "Корабль подлетел к астероидам! (Времязатраты: %s ч.)" % delay)

	# Время затраченное на дробление астероида
	delay = rnd.randi_range(9, 15)
	yield(get_tree().create_timer(delay), "timeout")
	print("Тов. %s закончил дробить астероид! (Времязатраты: %s ч.)" % [player_name, delay])
	Net.send_message(mining_players[player_name],
				 "Корабль закончил дробить астероид! (Времязатраты: %s ч.)" % delay)

	# Время затраченное на сбор руды
	delay = rnd.randi_range(6, 9)
	yield(get_tree().create_timer(delay), "timeout")
	print("Корабль тов. %s собрал минералы, добыто: %s кг. (Времязатраты: %s ч.)" % [player_name, m, delay])
	Net.send_message(mining_players[player_name],
				 "Корабль собрал минералы, добыто: %s кг. (Времязатраты: %s ч.)" % [m, delay])
	
	# Время затраченное на возвращение корабля к станции
	delay = rnd.randi_range(21, 27)
	yield(get_tree().create_timer(delay), "timeout")
	print("Корабль тов. %s подлетел к станции! (Времязатраты: %s ч.)" % [player_name, delay])
	Net.send_message(mining_players[player_name],
				 "Корабль прибыл к станции! (Времязатраты: %s ч.)" % delay)

	# Время затраченное на стыковку
	delay = rnd.randi_range(9, 15)
	yield(get_tree().create_timer(delay), "timeout")
	print("Корабль тов. %s залетел на станцию! (Времязатраты: %s ч.)" % [player_name, delay])
	Net.send_message(mining_players[player_name],
				 "Корабль залетел на станцию! (Времязатраты: %s ч.)" % delay)

	# Время затраченное на выгрузку руды
	delay = rnd.randi_range(6, 9)
	yield(get_tree().create_timer(delay), "timeout")
	print("Тов. %s продал минералы за %s Инмо. (Времязатраты: %s ч.)" % [player_name, i, delay])
	Net.send_message(mining_players[player_name],
				 "Минералы проданы за %s Инмо. (Времязатраты: %s ч.)" % [i, delay])
	
	# Цикл добычи завершен, удаляем игрока из списка выполняющих задание
	mining_players.erase(player_name)


# Обработчик сигнала mining_exec
# Запуск процесса добычи руды. Вызывается игроком
func start_mining(id):
	# Получаем его имя
	var caller_name = Net.online_players[id]
	
	# проверяем не запускал ли игрок добычу руды ранее (корабль на задании).
	# если так, то выходим из процедуры
	if mining_players.has(caller_name):
		Net.send_message(id, 'Корабль на задании.')
		return
	
	# Добавляем игрока в список игроков осуществляющих добычу руды
	mining_players[caller_name] = id
	# Запускаем корабль к астероидам добывать руду
	Net.send_message(id, 'Полетное задание получено. Корабль готовится к вылету.')
	create_mining_task(caller_name)


# Обработчик сигнала player_registered.
# Вызывается при регистрации игрока на сервере.
# Здесь обрабатывается ситуация когда корабль подключившегося игрока добывает руду
# в автономном режиме.
func connect_miner(name, id):
	# Если корабль игрока добывает руду в автономном режиме, фиксируем его ID
	# в списке выполняющих добычу руды
	if mining_players.has(name):
		mining_players[name] = id


# Обработчик сигнала player_unregistered
# Вызывается при отключении игрока от сервера.
func disconnect_miner(name):
	# Если корабль игрока в процессе добывания руды, убираем его ID из списка
	# выполняющих эту задачу, чтобы сервер не пытался слать сообщения отключившемуся игроку
	if mining_players.has(name):
		mining_players[name] = -1


func _ready():
	# Инициализируем генератор случайных чисел
	rnd.randomize()
	# Регистрируем обработчик сигнала player_registered (-> connect_miner)
	if !Net.is_connected("player_registered", self, "connect_miner"):
		Net.connect("player_registered", self, "connect_miner")
	# Регистрируем обработчик сигнала player_unregistered (-> disconnect_miner)
	if !Net.is_connected("player_unregistered", self, "disconnect_miner"):
		Net.connect("player_unregistered", self, "disconnect_miner")
	# Регистрируем обработчик сигнала mining_exec (-> start_mining)
	if !Net.is_connected("mining_exec", self, "start_mining"):
		Net.connect("mining_exec", self, "start_mining")
