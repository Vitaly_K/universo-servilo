# Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!

Workers of the world, unite!

Пролетарии всех стран, соединяйтесь!

# Основная информация

Это репозиторий серверного приложения Реального виртуального мира Универсо (Universo) на Godot.

Смотрите список активных Обсуждений (Issues) здесь в репозитории https://gitlab.com/tehnokom/universo-servilo/-/issues

Более подробная информация в главном репозитории Универсо вот тут по ссылке https://gitlab.com/tehnokom/universo

Наш дискорд-сервер по Универсо https://discord.gg/bsvdPy5